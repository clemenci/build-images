#!/bin/bash -e

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

if ! grep -Fq "${USER_ID}" /etc/passwd ; then
  sed -i "s#^user:x:.*#${USER}:x:${USER_ID}:${GROUP_ID}::${HOME}:/bin/bash#" /etc/passwd
  sed -i 's#\buser\b#'${USER}'#g' /etc/group
fi

exec "$@"
