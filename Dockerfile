FROM cern/cc7-base

# Part of the code was inspired by
# https://github.com/eclipse/che-dockerfiles/blob/master/recipes/stack-base/centos

COPY ["packages.txt", "/root/packages.txt"]
RUN yum install -y http://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm && \
    yum update -y && \
    grep -v '^#' /root/packages.txt | xargs yum install -y && \
    yum clean all && rm -rf /var/cache/yum /var/lib/yum/yumdb && \
    echo "\tGSSAPIDelegateCredentials yes" >> /etc/ssh/ssh_config && \
    echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    sed -i 's/requiretty/!requiretty/g' /etc/sudoers && \
    chmod 777 -R /tmp && chmod o+t -R /tmp && \
    useradd -u 1000 -G users,wheel,root -d /home/user --shell /bin/bash -m user && \
    usermod -p "*" user && \
    chmod g+w /etc/passwd /etc/group && \
    mkdir -p --mode=775 /workspace && \
    chown 1000:0 /workspace && \
    rm -f /etc/security/limits.d/*-nproc.conf

COPY ["entrypoint.sh", "/bin/entrypoint"]

USER user
WORKDIR /workspace
ENV HOME /home/user

ENTRYPOINT ["/bin/entrypoint"]

CMD ["/bin/bash"]
